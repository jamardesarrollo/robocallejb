package com.ts.jamar.exceptions;

public enum RobocallExceptionEnum {
	
	GENERIC (1, "Ha ocurrido un error en el sistema. "),
	INVALID_PARAMS (2, "Parametros invalidos. "),
	CAN_NOT_PERFORM_OPERATION (3, "No se puede ejecutar la operación. "),
	NO_RESULT (4, "No se encontraron resultados ");
	
	private final int code;
	private final String message;
	
	private RobocallExceptionEnum(int code, String message){
		this.code=code;
		this.message=message;
	}
	
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	
	
}
