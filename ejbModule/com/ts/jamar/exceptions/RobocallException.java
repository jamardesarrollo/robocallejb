package com.ts.jamar.exceptions;

public class RobocallException extends Exception {

	protected RobocallExceptionEnum errorEnum;
	
	public RobocallException(RobocallExceptionEnum errorEnum){
		super(errorEnum.getMessage());
		this.errorEnum=errorEnum;
	} 
	
	public RobocallException(RobocallExceptionEnum errorEnum, Exception ex){
		super(errorEnum.getMessage(),ex);
		this.errorEnum=errorEnum;
	}
	
	public RobocallException(String message,RobocallExceptionEnum errorEnum, Exception ex){
		super(errorEnum.getMessage()+" "+message,ex);
		this.errorEnum=errorEnum;
	}
	
	public RobocallException(String message,RobocallExceptionEnum errorEnum){
		super(errorEnum.getMessage()+" "+message);
		this.errorEnum=errorEnum;
	}

	/**
	 * @return the errorEnum
	 */
	public RobocallExceptionEnum getErrorEnum() {
		return errorEnum;
	}

	/**
	 * @param errorEnum the errorEnum to set
	 */
	public void setErrorEnum(RobocallExceptionEnum errorEnum) {
		this.errorEnum = errorEnum;
	}
	
	
}
