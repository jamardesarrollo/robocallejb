/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.util;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/*Librer�as para trabajar con archivos excel*/
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author DevilWandL
 */
public class ExcelUtil {


    
    public File toExcel (String nom_user, String[] cabecera, List<Object[]> contenido) throws IOException {
        java.util.Date fecha = new Date();

        /*La ruta donde se creará el archivo*/
        String rutaArchivo = System.getProperty("user.home") + "/"+nom_user+fecha.getTime()+".xls";
        /*Se crea el objeto de tipo File con la ruta del archivo*/
        File archivoXLS = new File(rutaArchivo);
        /*Si el archivo existe se elimina*/
        if (archivoXLS.exists()) {
            archivoXLS.delete();
        }
        /*Se crea el archivo*/
        archivoXLS.createNewFile();

        /*Se crea el libro de excel usando el objeto de tipo Workbook*/
        Workbook libro = new HSSFWorkbook();
        /*Se inicializa el flujo de datos con el archivo xls*/
        FileOutputStream archivo = new FileOutputStream(archivoXLS);

        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
        Sheet hoja = libro.createSheet("Hoja #1");

        /*La clase Row nos permitirá crear las filas*/
        Row fila = hoja.createRow(0);

        /*Cada fila tendrá x celdas de datos*/
        for (int c = 0; c < cabecera.length; c++) {
            /*Creamos la celda a partir de la fila actual*/
            Cell celda = fila.createCell(c);

            /*Si la fila es la número 0, estableceremos los encabezados*/
            celda.setCellValue(cabecera[c]);

        }

        int posicion = 1;
        for (Object[] item : contenido) {
            /*La clase Row nos permitirá crear las filas*/
            fila = hoja.createRow(posicion);

            /*Cada fila tendrá 5 celdas de datos*/
            for (int c = 0; c < cabecera.length; c++) {
                /*Creamos la celda a partir de la fila actual*/
                Cell celda = fila.createCell(c);

                /*Si no es la primera fila establecemos un valor*/
                if(item[c]==null)
                	celda.setCellValue("");
                else
                	celda.setCellValue(item[c].toString());

            }
            posicion++;
        }


        /*Escribimos en el libro*/
        libro.write(archivo);
        /*Cerramos el flujo de datos*/
        archivo.close();
        /*Y abrimos el archivo con la clase Desktop*/
        //Desktop.getDesktop().open(archivoXLS);
        
        return archivoXLS;
    }
    
    

}
