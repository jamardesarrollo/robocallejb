package com.ts.jamar.util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ts.jamar.entities.RbcCampanaCarga;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;


public class ReadFileCampanaCargaCSV {

    // Propiedades
    private char separador;
    private char comillas;
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String REGEX_PATTERN = "([0-9][0-9]{5,}+)?";

    // Constructor
    /**
     * Inicializa el constructor definiendo el separador de los campos y las
     * comillas usadas
     *
     * @param separador
     * @param comillas
     */
    public ReadFileCampanaCargaCSV(char separador, char comillas) {
        this.separador = separador;
        this.comillas = comillas;
    }

    // M�todos
    /**
     * Lee un CSV que no contiene el mismo caracter que el separador en su texto
     * y sin comillas que delimiten los campos
     *
     * @param path Ruta donde est� el archivo
     * @throws IOException
     */
    public List<RbcCampanaCarga> readCSV(String path) throws RobocallException {
    	BufferedReader bufferLectura = null;
    	try{
    	
	        List<RbcCampanaCarga> campanaCargas=new ArrayList<RbcCampanaCarga>();
	
	        // Abro el .csv en buffer de lectura
	        bufferLectura = new BufferedReader(new FileReader(path));
	
	        // Leo una l�nea del archivo
	        String linea = bufferLectura.readLine();
	        int count=0;
	        RbcCampanaCarga campanaCarga;
	
	        while (linea != null) {
	        	count++;
	            linea = linea.replaceAll(String.valueOf(this.comillas), "");
	
	            // Separa la l�nea le�da con el separador definido previamente
	            String[] campos = linea.split(String.valueOf(this.separador));
	
	            System.out.println(linea);
	            
	            if (!((campos.length == 7) && (!campos[0].isEmpty()) && (!campos[1].isEmpty()) && (!campos[6].isEmpty())))
	            	throw new RobocallException("Error en la linea "+count+": Archivo esta incorrecto",RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	            
	                //nombre mayor a 3 caracteres
	                if (!(campos[0].length() > 3)) 
	                	throw new RobocallException("Error en la linea "+count+": El nombre del cliente debe tener minimo 3 caracteres. ",
	                			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	
	                if ((!validatephone(campos[1]))) 
	                  	throw new RobocallException("Error en la linea "+count+": el telefono 1 no en v�lido. ",
	                			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	
	                if (!campos[2].isEmpty()) 
	                    if ((!validatephone(campos[2])))
	                    	throw new RobocallException("Error en la linea "+count+": el telefono 2 no en v�lido. ",
	                    			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	                
	                if (!campos[3].isEmpty()) 
	                    if ((!validatephone(campos[3]))) 
	                    	throw new RobocallException("Error en la linea "+count+": el telefono 3 no en v�lido. ",
	                    			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	                    
	                if (!campos[4].isEmpty()) 
	                    if ((!validatephone(campos[4]))) 
	                    	throw new RobocallException("Error en la linea "+count+": el telefono 4 no en v�lido. ",
	                    			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	                    
	                if (!campos[5].isEmpty()) 
	                    if ((!validatephone(campos[5]))) 
	                    	throw new RobocallException("Error en la linea "+count+": el telefono 5 no en v�lido. ",
	                    			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	                    
	                if ((!validateEmail(campos[6]))) 
	                	throw new RobocallException("Error en la linea "+count+": el correo el�ctronico no en v�lido. ",
	                			RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION);
	
	            
	            //System.out.println(Arrays.toString(campos));
	
	            // Vuelvo a leer del fichero
	            linea = bufferLectura.readLine();
	            
	            campanaCarga=new RbcCampanaCarga();
	            campanaCarga.setNombre(campos[0]);
	            campanaCarga.setTelefono1(campos[1]);
	            campanaCarga.setTelefono2(campos[2]);
	            campanaCarga.setTelefono3(campos[3]);
	            campanaCarga.setTelefono4(campos[4]);
	            campanaCarga.setTelefono5(campos[5]);
	            campanaCarga.setCorreo(campos[6]);
	            campanaCarga.setActivo(true);
	            
	            campanaCargas.add(campanaCarga);
	        }

	        return campanaCargas;
		}catch(RobocallException ex){
    		throw ex;
    		
		}catch(IOException ex){
    		throw new RobocallException("Error leyendo el archivo. ",RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,ex);
    		
		}catch(Exception ex){
    		throw new RobocallException(RobocallExceptionEnum.GENERIC,ex);
    	}finally {
    		 // CIerro el buffer de lectura
    		try{
    			if (bufferLectura != null) 
    	            bufferLectura.close();
    			
    		}catch(IOException ex){
    			ex.printStackTrace();
    		} 
	        
		}
    }

    /**
     * Validate given email with regular expression. 
     *
     * @param email email for validation
     * @return true valid email, otherwise false
     */
    public static boolean validateEmail(String email) {
        System.out.println("entro a validar");
        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        System.out.println(matcher.matches());
        return matcher.matches();

    }

    /**
     * Validate given email with regular expression.
     *
     * @param phone email for validation
     * @return true valid email, otherwise false
     */
    public static boolean validatephone(String phone) {

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(REGEX_PATTERN);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(phone);
        System.out.println(matcher.matches());
        return matcher.matches();

    }

}
