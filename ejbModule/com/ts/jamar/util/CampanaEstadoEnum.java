package com.ts.jamar.util;

public enum CampanaEstadoEnum {
	
	CREATE		(1),
	INITIATED 	(2),
	FINALIZED	(3);
	private  final int code;
	
	private CampanaEstadoEnum(int code){
		this.code=code;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	
	
	
}
