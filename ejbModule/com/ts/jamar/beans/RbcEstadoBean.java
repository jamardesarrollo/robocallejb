package com.ts.jamar.beans;

import javax.ejb.Stateless;

import com.ts.jamar.dao.impl.GenericDaoImpl;
import com.ts.jamar.entities.RbcEstado;

/**
 * Session Bean implementation class RbcEstadoBean
 */
@Stateless
public class RbcEstadoBean extends GenericDaoImpl<RbcEstado> implements RbcEstadoLocal {

	public RbcEstadoBean() {
		super(RbcEstado.class);
	}
       
	
   
}
