package com.ts.jamar.beans;

import java.util.List;

import javax.ejb.Local;

import com.ts.jamar.dao.GenericDao;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;

@Local
public interface RbcCamapanaLocal extends GenericDao<RbcCampana>{
	
	List<RbcCampana> findAll(String codigoEmpresa) throws RobocallException;

}
