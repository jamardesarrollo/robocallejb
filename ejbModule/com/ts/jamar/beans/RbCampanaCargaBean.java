package com.ts.jamar.beans;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import com.ts.jamar.dao.impl.GenericDaoImpl;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.entities.RbcCampanaCarga;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;

/**
 * Session Bean implementation class RbCampanaCargaBean
 */
@Stateless
public class RbCampanaCargaBean extends GenericDaoImpl<RbcCampanaCarga> implements RbCampanaCargaLocal {

	public RbCampanaCargaBean() {
		super(RbcCampanaCarga.class);
	}

	@Override
	public List findCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try {
			if(codigoEmpresa==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			List<RbcCampana> list=  this.getEm(codigoEmpresa)
					            .createNamedQuery("RbcCampanaCarga.findCustomAnswer")
					              .setParameter("1", codigoEmpresa)
					                .setParameter("2", cons_campana)
					                  .setParameter("3",resetInitialDate(intialDate))
					                    .setParameter("4", resetFinalDate(finalDate) )
						                  .setParameter("5", codigoEmpresa)
						                    .setParameter("6", cons_campana)
						                     .setParameter("7",resetInitialDate(intialDate))
						                      .setParameter("8", resetFinalDate(finalDate) )
						                      	.setParameter("9", codigoEmpresa)
						                      		.setParameter("10", cons_campana)
						                      			.setParameter("11",resetInitialDate(intialDate))
						                      			.setParameter("12", resetFinalDate(finalDate) )
					                .getResultList();
			
			if(list.isEmpty())
				throw new RobocallException(RobocallExceptionEnum.NO_RESULT);
			
			return list;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
	}

	@Override
	public List findSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try {
			if(codigoEmpresa==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			List<RbcCampana> list=  this.getEm(codigoEmpresa)
					            .createNamedQuery("RbcCampanaCarga.findSourceAnswer")
					              .setParameter("1", codigoEmpresa)
					                .setParameter("2", cons_campana)
					                .setParameter("3",resetInitialDate(intialDate))
					                .setParameter("4", resetFinalDate(finalDate) )
					                  .setParameter("5", codigoEmpresa)
						                .setParameter("6", cons_campana)
						                .setParameter("7",resetInitialDate(intialDate))
						                .setParameter("8", resetFinalDate(finalDate) )
						                  .setParameter("9", codigoEmpresa)
							                .setParameter("10", cons_campana)
							                .setParameter("11",resetInitialDate(intialDate))
							                .setParameter("12", resetFinalDate(finalDate) )
					                .getResultList();
			
			if(list.isEmpty())
				throw new RobocallException(RobocallExceptionEnum.NO_RESULT);
			
			return list;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
	}
	
	private Date resetInitialDate(Date date){
		Calendar c=Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		return c.getTime();
		
	}
	
	
	private Date resetFinalDate(Date date){
		Calendar c=Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		
		return c.getTime();
		
	}

	@Override
	public List lastAttempts(String codigoEmpresa, BigInteger cons_campana , Date intialDate, Date finalDate) throws RobocallException {
		try {
			if(codigoEmpresa==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			System.out.println("codigoEmpresa => "+codigoEmpresa);
			System.out.println("cons_campana => "+cons_campana);
			System.out.println("intialDate => "+resetInitialDate(intialDate));
			System.out.println("finalDate => "+resetFinalDate(finalDate));
		
			
			List list=  this.getEm(codigoEmpresa)
				            .createNamedQuery("RbcCampanaCarga.lastAttempts")
				              .setParameter("1", codigoEmpresa)
				                .setParameter("2", cons_campana)
					              .setParameter("3",resetInitialDate(intialDate))
					                .setParameter("4", resetFinalDate(finalDate) )
				                .getResultList();
			
			System.out.println("lista => "+list.size());
			
			if(list.isEmpty())
				throw new RobocallException(RobocallExceptionEnum.NO_RESULT);
			
			return list;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}

	}

	@Override
	public List<RbcCampanaCarga> findByCEmp(String codigoEmpresa) throws RobocallException {
		
		try {
			if(codigoEmpresa==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			List<RbcCampanaCarga> list=  this.getEm(codigoEmpresa)
				            .createNamedQuery("RbcCampanaCarga.findByCEmp")
				              .setParameter("cEmp", codigoEmpresa)
				                .getResultList();
			
			if(list.isEmpty())
				throw new RobocallException(RobocallExceptionEnum.NO_RESULT);
			
			return list;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
	}
       
   

}
