package com.ts.jamar.beans;

import javax.ejb.Local;

import com.ts.jamar.dao.GenericDao;
import com.ts.jamar.entities.RbcCampanaEstado;

@Local
public interface RbcCampanaEstadoLocal extends GenericDao<RbcCampanaEstado>{

}
