package com.ts.jamar.beans;

import javax.ejb.Stateless;

import com.ts.jamar.dao.impl.GenericDaoImpl;
import com.ts.jamar.entities.RbcCampanaEstado;

/**
 * Session Bean implementation class RbcCampanaEstadoBean
 */
@Stateless
public class RbcCampanaEstadoBean extends GenericDaoImpl<RbcCampanaEstado> implements RbcCampanaEstadoLocal {

	public RbcCampanaEstadoBean() {
		super(RbcCampanaEstado.class);
	}
       
   

}
