package com.ts.jamar.beans;

import java.util.List;

import javax.ejb.Stateless;

import com.ts.jamar.dao.impl.GenericDaoImpl;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;

/**
 * Session Bean implementation class RbcCamapana
 */
@Stateless
public class RbcCamapanaBean extends GenericDaoImpl<RbcCampana> implements RbcCamapanaLocal {

	public RbcCamapanaBean() {
		super(RbcCampana.class);
	}

	@Override
	public List<RbcCampana> findAll(String codigoEmpresa)throws RobocallException{
		try {
			if(codigoEmpresa==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			List<RbcCampana> list=  this.getEm(codigoEmpresa)
					            .createNamedQuery("RbcCampana.findAll")
					              .setParameter("cEmp", codigoEmpresa)
					                .getResultList();
			
			if(list.isEmpty())
				throw new RobocallException(RobocallExceptionEnum.NO_RESULT);
			
			return list;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
	}
       
}
