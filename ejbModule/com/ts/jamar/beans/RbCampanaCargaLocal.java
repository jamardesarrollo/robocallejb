package com.ts.jamar.beans;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ts.jamar.dao.GenericDao;
import com.ts.jamar.entities.RbcCampanaCarga;
import com.ts.jamar.exceptions.RobocallException;

@Local
public interface RbCampanaCargaLocal extends GenericDao<RbcCampanaCarga> {

	List findCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	List findSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	List lastAttempts(String codigoEmpresa, BigInteger cons_campana , Date intialDate, Date finalDate) throws RobocallException;
	
	List<RbcCampanaCarga> findByCEmp(String codigoEmpresa) throws RobocallException;
	
	
}
