package com.ts.jamar.beans;

import javax.ejb.Local;

import com.ts.jamar.dao.GenericDao;
import com.ts.jamar.entities.RbcEstado;

@Local
public interface RbcEstadoLocal extends GenericDao<RbcEstado> {

}
