package com.ts.jamar.dao.impl;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import com.ts.jamar.dao.GenericDao;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;

public class GenericDaoImpl<T> implements GenericDao<T> {
	
	protected Class<T> type;
	
	String codigoEmpresa="";
	
	@Resource
	SessionContext sessionContext;
	
	@PersistenceContext(unitName="RobocallEJB_JA")
	protected EntityManager em2;
    
    @PersistenceContext(unitName="RobocallEJB_JP")
    protected EntityManager em3;

	@PersistenceContext(unitName="RobocallEJB_default")
	protected EntityManager em;
	
	public String getCodigoEmpresa(){
		return codigoEmpresa;
	}
	
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;		
	}
	
	public EntityManager getEm(String codigoEmpresa)
	{
		if(codigoEmpresa.equals("JP")){
			return em3;
		}else if(codigoEmpresa.equals("JA")){
			return em2;
		}else{
			return em;
		}
	}

	public GenericDaoImpl(Class<T> type) {
		this.type = type;
	}


	public T save(String codigoEmpresa, T entity) throws RobocallException{
		try {
			if(entity==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			getEm(codigoEmpresa).persist(entity);
			getEm(codigoEmpresa).flush();
			return entity;
			
		} catch (RobocallException e) {
			e.printStackTrace();
			throw e;
			
		} catch (EntityExistsException e) {
			e.printStackTrace();
			throw new RobocallException("El registro ya existe. ",RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
		
	}


	public Boolean delete(String codigoEmpresa,T entity)throws RobocallException {

		try {
			if(entity==null)
				throw new RobocallException(RobocallExceptionEnum.INVALID_PARAMS);
			
			getEm(codigoEmpresa).remove(entity);
			return true;
			
		} catch (RobocallException e) {
			throw e;
			
		} catch (IllegalStateException e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
			
		} catch (Exception e) {
			throw new RobocallException(RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);
		}
	}


	public T edit(String codigoEmpresa,T entity) {
		// TODO Auto-generated method stub
		try {
			return getEm(codigoEmpresa).merge(entity);
		} catch (Exception ex) {
			return null;
		}
	}


//	public T find(String codigoEmpresa,Object id) throws RobocallException{
//		// TODO Auto-generated method stub
//		return (T) getEm(codigoEmpresa).find(type, id);
//		
//		
//	}
	
	public T find(String codigoEmpresa,Object id)throws RobocallException {
		try{
			if(codigoEmpresa==null || id==null)
				throw new RobocallException (RobocallExceptionEnum.INVALID_PARAMS);
			
			return (T)this.getEm(codigoEmpresa).createNamedQuery(this.type.getSimpleName()+".findByPk")
			      .setParameter("cEmp", codigoEmpresa)
			       .setParameter("cons", id)
			         .getSingleResult();
		
		} catch(RobocallException ex){
			throw ex;
		
		}catch(NoResultException ex){
			throw new RobocallException (RobocallExceptionEnum.NO_RESULT,ex);
			
		}catch(NonUniqueResultException ex){
			throw new RobocallException (RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,ex);
			
		}catch(Exception ex){
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);
		}
	}

}
