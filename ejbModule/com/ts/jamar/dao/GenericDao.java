package com.ts.jamar.dao;

import com.ts.jamar.exceptions.RobocallException;

public interface GenericDao<T> {
	 	public T save(String codigoEmpresa, T entity) throws RobocallException;
	    public Boolean delete(String codigoEmpresa,T entity)throws RobocallException;
	    public T edit(String codigoEmpresa,T entity)throws RobocallException;
	    public T find(String codigoEmpresa, Object id)throws RobocallException;
}
