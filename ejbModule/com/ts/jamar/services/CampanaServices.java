package com.ts.jamar.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.poi.util.IOUtils;

import com.ts.jamar.beans.RbCampanaCargaLocal;
import com.ts.jamar.beans.RbcCamapanaLocal;
import com.ts.jamar.beans.RbcCampanaEstadoLocal;
import com.ts.jamar.beans.RbcEstadoLocal;
import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.entities.RbcCampanaCarga;
import com.ts.jamar.entities.RbcCampanaCargaPK;
import com.ts.jamar.entities.RbcCampanaEstado;
import com.ts.jamar.entities.RbcCampanaEstadosPK;
import com.ts.jamar.entities.RbcCampanaPK;
import com.ts.jamar.entities.RbcEstado;
import com.ts.jamar.exceptions.RobocallException;
import com.ts.jamar.exceptions.RobocallExceptionEnum;
import com.ts.jamar.util.CampanaEstadoEnum;
import com.ts.jamar.util.ExcelUtil;
import com.ts.jamar.util.ReadFileCampanaCargaCSV;

/**
 * Session Bean implementation class CampanaServices
 */
@Stateless
public class CampanaServices implements CampanaServicesLocal {

	@EJB
	private RbcCamapanaLocal rbcCamapanaLocal;
	
	@EJB
	private RbCampanaCargaLocal  rbcCampanaCargaLocal;
	
    @EJB
	private RbcCampanaEstadoLocal rbcCampanaEstadoLocal;
	
	@EJB
	private RbcEstadoLocal rbcEstadoLocal;
	
	@Override
	public void saveCampana(String codigoEmpresa,
							String usuario, 
							RbcCampana campana,
							InputStream audio, 
							InputStream file,
							String nameAudio, 
							String nameFile) throws RobocallException {
		try{
			
			RbcCampanaPK pk=new RbcCampanaPK();
			pk.setCEmp(codigoEmpresa);
			
			RbcCampana rb=new RbcCampana();
			rb.setFecha(new Date());
			rb.setDescripcion(campana.getDescripcion());
			rb.setEnviarCorreo(campana.getEnviarCorreo()==null ? false : campana.getEnviarCorreo());
			rb.setFechaInicio(campana.getFechaInicio());
			rb.setIntentosTelefono1(campana.getIntentosTelefono1()==null ? 0 : campana.getIntentosTelefono1());
			rb.setIntentosTelefono2(campana.getIntentosTelefono2()==null ? 0 : campana.getIntentosTelefono2());
			rb.setIntentosTelefono3(campana.getIntentosTelefono3()==null ? 0 : campana.getIntentosTelefono3());
			rb.setIntentosTelefono4(campana.getIntentosTelefono4()==null ? 0 : campana.getIntentosTelefono4());
			rb.setIntentosTelefono5(campana.getIntentosTelefono5()==null ? 0 : campana.getIntentosTelefono5());
			rb.setNombre(campana.getNombre());
			/**
			 * Se procede a invocar el
			 * metodo para crear el
			 * archivo de audio de la campaña.
			 */
			if(audio!=null){
				File fileAudio = this.createFileCampaña(audio, nameAudio);
				
				System.out.println("ruta absoluta de archivo "+fileAudio.getAbsolutePath());
				System.out.println("ruta path de archivo "+fileAudio.getPath());
				
				rb.setRutaAudio(fileAudio.getAbsolutePath());
			}
			rb.setTextoNoCorreo(campana.getTextoNoCorreo());
			rb.setTextoPreguntaCorreo(campana.getTextoPreguntaCorreo());
			rb.setTextoSiCorreo(campana.getTextoSiCorreo());
			rb.setTimbradoTelefono1(campana.getTimbradoTelefono1()==null ? 0 : campana.getTimbradoTelefono1());
			rb.setTimbradoTelefono2(campana.getTimbradoTelefono2()==null ? 0 : campana.getTimbradoTelefono2());
			rb.setTimbradoTelefono3(campana.getTimbradoTelefono3()==null ? 0 : campana.getTimbradoTelefono3());
			rb.setTimbradoTelefono4(campana.getTimbradoTelefono4()==null ? 0 : campana.getTimbradoTelefono4());
			rb.setTimbradoTelefono5(campana.getTimbradoTelefono5()==null ? 0 : campana.getTimbradoTelefono5());
			rb.setUsuario(usuario);
			rb.setRbcCampanaPK(pk);
			
			
			RbcEstado estado=this.rbcEstadoLocal.find(codigoEmpresa, CampanaEstadoEnum.CREATE.getCode());
			rb.setRbcEstados(estado);
			
			this.rbcCamapanaLocal.save(codigoEmpresa, rb);
			
			RbcCampanaEstado campanaEstado=new RbcCampanaEstado();
			
			RbcCampanaEstadosPK pkCampanaEstado=new RbcCampanaEstadosPK();
			pkCampanaEstado.setCEmp(codigoEmpresa);
			
			campanaEstado.setFecha(new Date());
			campanaEstado.setRbcCampana(rb);
			campanaEstado.setUsuario(usuario);
			campanaEstado.setRbcEstados(estado);
			campanaEstado.setRbcCampanaEstadosPK(pkCampanaEstado);
			
			this.rbcCampanaEstadoLocal.save(codigoEmpresa, campanaEstado);
			rb.setRbcEstados(estado);
			rb.setRbcCampanaEstados(campanaEstado);
			
			this.rbcCamapanaLocal.edit(codigoEmpresa, rb);
			
			System.out.println("rb => "+rb);
			this.rbcCamapanaLocal.save(codigoEmpresa, rb);
			
			/**
			 * Se recorre el archivo excel 
			 * a guardar en base de datos.
			 */
			if(file!=null)
			   this.saveLoadExcel(codigoEmpresa,usuario,rb,  file,  nameFile);
				
			
		}catch(RobocallException ex){
			throw ex;
			
		}catch(Exception ex){
			throw new RobocallException(RobocallExceptionEnum.GENERIC,ex);
		}
		
	}
	
	private void saveLoadExcel(String codigoEmpresa,String usuario, RbcCampana rb, InputStream file, String nameFile) 
			throws FileNotFoundException, IOException, RobocallException{
		File fileCSV = this.createFileCampaña(file, nameFile);
		
		System.out.println("ruta absoluta de archivo CSV "+fileCSV.getAbsolutePath());
		System.out.println("ruta path de archivo CSV "+fileCSV.getPath());
		
		
		ReadFileCampanaCargaCSV readFile=new ReadFileCampanaCargaCSV(';','"');
		List<RbcCampanaCarga> list=readFile.readCSV(fileCSV.getAbsolutePath());
		
		for (int i = 0; i < list.size(); i++) {
			/*
			 * Esto conforma la llave primaria, le
			 * pasamos en codigo de la empresa
			 * y el secuencial se settea solo
			 * */
			RbcCampanaCargaPK pk1=new RbcCampanaCargaPK();
			pk1.setCEmp(codigoEmpresa);
			
			list.get(i).setUsuario(usuario);
			list.get(i).setRbcCampanaCargaPK(pk1);
			list.get(i).setRbcCampana(rb);
			list.get(i).setFecha(new Date());
			
			this.rbcCampanaCargaLocal.save(codigoEmpresa, list.get(i));
		}
	}

	/**
	 * Metodo implementado para guardar
	 * el audio seleccionado.
	 * 
	 * @param audio
	 * @param nameAudio
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private File createFileCampaña(InputStream file, String nameFile) throws IOException, FileNotFoundException {
		
		Date fecha=new Date();
		String rutaArchivo=System.getProperty("user.home")+"/"+fecha.getTime()+nameFile;
		File fileNew=new File(rutaArchivo);
		if(!fileNew.exists())
			fileNew.createNewFile();
		
		FileOutputStream fop=new FileOutputStream(fileNew);
		fop.write(IOUtils.toByteArray(file));
		fop.flush();
		fop.close();
		
		return fileNew;
	}

	@Override
	public RbcCampana findCampana(String codigoEmpresa, BigInteger cons) throws RobocallException {
		try{
			return this.rbcCamapanaLocal.find(codigoEmpresa, cons);
			
		}catch(RobocallException ex){
			throw ex;
			
		}catch(Exception ex){
			throw new RobocallException(RobocallExceptionEnum.GENERIC,ex);
		}	
	}

	
	@Override
	public void editCampana(String codigoEmpresa, String usuario, RbcCampana campana, InputStream audio, InputStream file,  String nameAudio, String nameFile) throws RobocallException {
		try{
			
			RbcCampana rb=this.rbcCamapanaLocal.find(codigoEmpresa, campana.getRbcCampanaPK().getCons());
			rb.setDescripcion(campana.getDescripcion());
			rb.setEnviarCorreo(campana.getEnviarCorreo()==null ? false : campana.getEnviarCorreo());
			rb.setFechaInicio(campana.getFechaInicio());
			rb.setIntentosTelefono1(campana.getIntentosTelefono1()==null ? 0 : campana.getIntentosTelefono1());
			rb.setIntentosTelefono2(campana.getIntentosTelefono2()==null ? 0 : campana.getIntentosTelefono2());
			rb.setIntentosTelefono3(campana.getIntentosTelefono3()==null ? 0 : campana.getIntentosTelefono3());
			rb.setIntentosTelefono4(campana.getIntentosTelefono4()==null ? 0 : campana.getIntentosTelefono4());
			rb.setIntentosTelefono5(campana.getIntentosTelefono5()==null ? 0 : campana.getIntentosTelefono5());
			rb.setNombre(campana.getNombre());
			/**
			 * Se crea el archivo audio seleccionado
			 */
			if(audio!=null){
				File fileAudio = this.createFileCampaña(audio,nameAudio);
				rb.setRutaAudio(fileAudio.getAbsolutePath());
			}
			rb.setTextoNoCorreo(campana.getTextoNoCorreo());
			rb.setTextoPreguntaCorreo(campana.getTextoPreguntaCorreo());
			rb.setTextoSiCorreo(campana.getTextoSiCorreo());
			rb.setTimbradoTelefono1(campana.getTimbradoTelefono1()==null ? 0 : campana.getTimbradoTelefono1());
			rb.setTimbradoTelefono2(campana.getTimbradoTelefono2()==null ? 0 : campana.getTimbradoTelefono2());
			rb.setTimbradoTelefono3(campana.getTimbradoTelefono3()==null ? 0 : campana.getTimbradoTelefono3());
			rb.setTimbradoTelefono4(campana.getTimbradoTelefono4()==null ? 0 : campana.getTimbradoTelefono4());
			rb.setTimbradoTelefono5(campana.getTimbradoTelefono5()==null ? 0 : campana.getTimbradoTelefono5());
				
			this.rbcCamapanaLocal.edit(codigoEmpresa, rb);
			
			if(file!=null){
				try{
					List<RbcCampanaCarga> list=this.rbcCampanaCargaLocal.findByCEmp(codigoEmpresa);
					for (int i = 0; i < list.size(); i++) {
						list.get(i).setActivo(false);
						this.rbcCampanaCargaLocal.edit(codigoEmpresa, list.get(i));
					}
				}catch(RobocallException ex){
					if(!ex.getErrorEnum().equals(RobocallExceptionEnum.NO_RESULT))
						throw ex;
				}
				
				
				this.saveLoadExcel(codigoEmpresa,usuario,rb,  file,  nameFile);
			}
			
			
		}catch(RobocallException ex){
			throw ex;
			
		}catch(Exception ex){
			throw new RobocallException(RobocallExceptionEnum.GENERIC,ex);
		}
		
		
	}

	@Override
	public void startCampana(String codigoEmpresa, String usuario, RbcCampana campana) throws RobocallException {
		try{
			
			RbcCampana rb=this.rbcCamapanaLocal.find(codigoEmpresa, campana.getRbcCampanaPK().getCons());
			
			RbcEstado estado=this.rbcEstadoLocal.find(codigoEmpresa, CampanaEstadoEnum.INITIATED.getCode());			
			RbcCampanaEstado campanaEstado=new RbcCampanaEstado();
			RbcCampanaEstadosPK pkCampanaEstado=new RbcCampanaEstadosPK();
			pkCampanaEstado.setCEmp(codigoEmpresa);
			
			campanaEstado.setFecha(new Date());
			campanaEstado.setRbcCampana(rb);
			campanaEstado.setUsuario(usuario);
			campanaEstado.setRbcEstados(estado);
			campanaEstado.setRbcCampanaEstadosPK(pkCampanaEstado);
			
			this.rbcCampanaEstadoLocal.save(codigoEmpresa, campanaEstado);
			
			rb.setRbcCampanaEstados(campanaEstado);
			rb.setRbcEstados(estado);
			
			this.rbcCamapanaLocal.edit(codigoEmpresa, rb);
			
		}catch(RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch(Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);
		}
		
	}

	@Override
	public List findCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try{
			return this.rbcCampanaCargaLocal.findCustomAnswer(codigoEmpresa, cons_campana, intialDate, finalDate);
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public List findSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try{
			return this.rbcCampanaCargaLocal.findSourceAnswer(codigoEmpresa, cons_campana, intialDate, finalDate);
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public List lastAttempts(String codigoEmpresa, BigInteger cons_campana , Date intialDate, Date finalDate) throws RobocallException {
		try{
			return this.rbcCampanaCargaLocal.lastAttempts(codigoEmpresa, cons_campana , intialDate, finalDate);
		}catch( RobocallException ex){
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public List<RbcCampana> findCampanas(String codigoEmpresa) throws RobocallException {
		try{
			return this.rbcCamapanaLocal.findAll(codigoEmpresa);
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public File generateExcelCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try{
			String[] cabecera = new String[2];
			cabecera[0]="cantidad_clientes";
			cabecera[1]="cantidad_exito";
			cabecera[2]="cantidad_medio_exito";
			return generateExcel("",cabecera , this.rbcCampanaCargaLocal.findCustomAnswer(codigoEmpresa, cons_campana, intialDate, finalDate));
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public File generateExcelSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try{
			String[] cabecera = new String[3];
			cabecera[0]="cantidad_clientes";
			cabecera[1]="cantidad_exito_llamada";
			cabecera[2]="cantidad_exito_correo";
			return generateExcel("",cabecera , this.rbcCampanaCargaLocal.findSourceAnswer(codigoEmpresa, cons_campana, intialDate, finalDate));
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	@Override
	public File generateExcelAttempts(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException {
		try{
			String[] cabecera = new String[4];
			cabecera[0]="NOMBRE";
			cabecera[1]="intentos";
			cabecera[2]="exito_llamada";
			cabecera[3]="fecha_ejecucion";
			
			List list=this.rbcCampanaCargaLocal.lastAttempts(codigoEmpresa, cons_campana, intialDate, finalDate);
			
			System.out.println("list= > "+list);
			File file= generateExcel("",cabecera ,list );
			System.out.println("file => "+file);
			return file;
			
		}catch( RobocallException ex){
			ex.printStackTrace();
			throw ex;
			
		}catch( Exception ex){
			ex.printStackTrace();
			throw new RobocallException (RobocallExceptionEnum.GENERIC,ex);	
			
		}
	}

	private File generateExcel(String usuario , String[] cabecera , List<Object[]> cuerpo)throws RobocallException{
	
		try {
			ExcelUtil util= new ExcelUtil();
			return util.toExcel(usuario, cabecera, cuerpo);
		} catch (IOException e) {
			throw new RobocallException ("No se pudo generar el excel. ",RobocallExceptionEnum.CAN_NOT_PERFORM_OPERATION,e);	
		}
	}
}
