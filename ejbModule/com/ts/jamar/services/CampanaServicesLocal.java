package com.ts.jamar.services;

import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.ts.jamar.entities.RbcCampana;
import com.ts.jamar.exceptions.RobocallException;

@Local
public interface CampanaServicesLocal {

	List<RbcCampana> findCampanas (String codigoEmpresa) throws RobocallException;
	
	void saveCampana(String codigoEmpresa,String usuario, RbcCampana campana,InputStream audio, InputStream file, String nameAudio, String nameFile) throws RobocallException;
	
	void editCampana(String codigoEmpresa, String usuario,RbcCampana campana,InputStream audio, InputStream file, String nameAudio, String nameFile)throws RobocallException;
	
	void startCampana(String codigoEmpresa, String usuario,RbcCampana campana)throws RobocallException;
	
	RbcCampana findCampana(String codigoEmpresa,BigInteger cons )throws RobocallException;
	
	List findCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	List findSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	List lastAttempts(String codigoEmpresa, BigInteger cons_campana , Date intialDate, Date finalDate) throws RobocallException;
	 
	File generateExcelCustomAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	File generateExcelSourceAnswer(String codigoEmpresa, BigInteger cons_campana, Date intialDate, Date finalDate) throws RobocallException;
	
	File generateExcelAttempts(String codigoEmpresa, BigInteger cons_campana , Date intialDate, Date finalDate) throws RobocallException;
	
	
}
