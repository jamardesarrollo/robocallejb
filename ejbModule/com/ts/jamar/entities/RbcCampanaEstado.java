/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TECHNISUPPORT
 */
@Entity
@Table(name = "RBC_CAMPANA_ESTADOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RbcCampanaEstado.findAll", query = "SELECT r FROM RbcCampanaEstado r"),
    @NamedQuery(name = "RbcCampanaEstado.findByCons", query = "SELECT r FROM RbcCampanaEstado r WHERE r.rbcCampanaEstadosPK.cons = :cons"),
    @NamedQuery(name = "RbcCampanaEstado.findByCEmp", query = "SELECT r FROM RbcCampanaEstado r WHERE r.rbcCampanaEstadosPK.cEmp = :cEmp"),
    @NamedQuery(name = "RbcCampanaEstado.findByFecha", query = "SELECT r FROM RbcCampanaEstado r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "RbcCampanaEstado.findByUsuario", query = "SELECT r FROM RbcCampanaEstado r WHERE r.usuario = :usuario")})
public class RbcCampanaEstado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbcCampanaEstadosPK rbcCampanaEstadosPK;
    @Basic(optional = false)
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @Column(name = "USUARIO")
    private String usuario;
    @JoinColumns({
        @JoinColumn(name = "CONS_CAMPANA_FK", referencedColumnName = "CONS"),
        @JoinColumn(name = "C_EMP", referencedColumnName = "C_EMP", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private RbcCampana rbcCampana;
    @JoinColumns({
        @JoinColumn(name = "C_EMP", referencedColumnName = "c_emp", insertable = false, updatable = false),
        @JoinColumn(name = "CONS_ESTADOS_FK", referencedColumnName = "CONS")})
    @ManyToOne(optional = false)
    private RbcEstado rbcEstados;

    public RbcCampanaEstado() {
    }

    public RbcCampanaEstado(RbcCampanaEstadosPK rbcCampanaEstadosPK) {
        this.rbcCampanaEstadosPK = rbcCampanaEstadosPK;
    }

    public RbcCampanaEstado(RbcCampanaEstadosPK rbcCampanaEstadosPK, Date fecha, String usuario) {
        this.rbcCampanaEstadosPK = rbcCampanaEstadosPK;
        this.fecha = fecha;
        this.usuario = usuario;
    }

    public RbcCampanaEstado(BigInteger cons, String cEmp) {
        this.rbcCampanaEstadosPK = new RbcCampanaEstadosPK(cons, cEmp);
    }

    public RbcCampanaEstadosPK getRbcCampanaEstadosPK() {
        return rbcCampanaEstadosPK;
    }

    public void setRbcCampanaEstadosPK(RbcCampanaEstadosPK rbcCampanaEstadosPK) {
        this.rbcCampanaEstadosPK = rbcCampanaEstadosPK;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public RbcCampana getRbcCampana() {
        return rbcCampana;
    }

    public void setRbcCampana(RbcCampana rbcCampana) {
        this.rbcCampana = rbcCampana;
    }

    public RbcEstado getRbcEstados() {
        return rbcEstados;
    }

    public void setRbcEstados(RbcEstado rbcEstados) {
        this.rbcEstados = rbcEstados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbcCampanaEstadosPK != null ? rbcCampanaEstadosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbcCampanaEstado)) {
            return false;
        }
        RbcCampanaEstado other = (RbcCampanaEstado) object;
        if ((this.rbcCampanaEstadosPK == null && other.rbcCampanaEstadosPK != null) || (this.rbcCampanaEstadosPK != null && !this.rbcCampanaEstadosPK.equals(other.rbcCampanaEstadosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.RbcCampanaEstados[ rbcCampanaEstadosPK=" + rbcCampanaEstadosPK + " ]";
    }
    
}
