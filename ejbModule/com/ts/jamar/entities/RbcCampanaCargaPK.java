/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.TableGenerator;

/**
 *
 * @author TECHNISUPPORT
 */
@Embeddable
public class RbcCampanaCargaPK implements Serializable {

	private static final long serialVersionUID = 1L;
	@TableGenerator(
			name="campanaCargaConsSeq",
			table="SECUENCIAS_COACHING",
			pkColumnName="SEC_LLAVE",
			valueColumnName="SEC_VALOR",
			pkColumnValue="CONS_CAMPANA_CARGA"
	)	
	@GeneratedValue(generator="campanaCargaConsSeq", strategy=GenerationType.TABLE)
    @Column(name = "CONS")
    private BigInteger cons;
    @Basic(optional = false)
    @Column(name = "C_EMP")
    private String cEmp;

    public RbcCampanaCargaPK() {
    }

    public RbcCampanaCargaPK(BigInteger cons, String cEmp) {
        this.cons = cons;
        this.cEmp = cEmp;
    }

    public BigInteger getCons() {
        return cons;
    }

    public void setCons(BigInteger cons) {
        this.cons = cons;
    }

    public String getCEmp() {
        return cEmp;
    }

    public void setCEmp(String cEmp) {
        this.cEmp = cEmp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cons != null ? cons.hashCode() : 0);
        hash += (cEmp != null ? cEmp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbcCampanaCargaPK)) {
            return false;
        }
        RbcCampanaCargaPK other = (RbcCampanaCargaPK) object;
        if ((this.cons == null && other.cons != null) || (this.cons != null && !this.cons.equals(other.cons))) {
            return false;
        }
        if ((this.cEmp == null && other.cEmp != null) || (this.cEmp != null && !this.cEmp.equals(other.cEmp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ts.faces.beans.RbcCampanaCargaPK[ cons=" + cons + ", cEmp=" + cEmp + " ]";
    }
    
}
