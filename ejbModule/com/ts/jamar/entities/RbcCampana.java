/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TECHNISUPPORT
 */
@Entity
@Table(name = "RBC_CAMPANA")
@XmlRootElement
@NamedQueries({
    
    @NamedQuery(name = "RbcCampana.findByCons", query = "SELECT r FROM RbcCampana r WHERE r.rbcCampanaPK.cons = :cons ORDER BY r.fecha"),
    @NamedQuery(name = "RbcCampana.findAll", query = "SELECT r FROM RbcCampana r WHERE r.rbcCampanaPK.cEmp = :cEmp ORDER BY r.fecha"),
    @NamedQuery(name = "RbcCampana.findByPk", query = "SELECT r FROM RbcCampana r WHERE r.rbcCampanaPK.cons = :cons AND r.rbcCampanaPK.cEmp = :cEmp ORDER BY r.fecha")
    })

public class RbcCampana implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbcCampanaPK rbcCampanaPK;
    @Column(name = "USUARIO")
    private String usuario;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "ENVIAR_CORREO")
    private Boolean enviarCorreo;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "RUTA_AUDIO")
    private String rutaAudio;
    @Column(name = "TEXTO_PREGUNTA_CORREO")
    private String textoPreguntaCorreo;
    @Column(name = "TEXTO_SI_CORREO")
    private String textoSiCorreo;
    @Column(name = "TEXTO_NO_CORREO")
    private String textoNoCorreo;
    @Column(name = "INTENTOS_TELEFONO_1")
    private Integer intentosTelefono1;
    @Column(name = "INTENTOS_TELEFONO_2")
    private Integer intentosTelefono2;
    @Column(name = "INTENTOS_TELEFONO_3")
    private Integer intentosTelefono3;
    @Column(name = "INTENTOS_TELEFONO_4")
    private Integer intentosTelefono4;
    @Column(name = "INTENTOS_TELEFONO_5")
    private Integer intentosTelefono5;
    @Column(name = "TIMBRADO_TELEFONO_1")
    private Integer timbradoTelefono1;
    @Column(name = "TIMBRADO_TELEFONO_2")
    private Integer timbradoTelefono2;
    @Column(name = "TIMBRADO_TELEFONO_3")
    private Integer timbradoTelefono3;
    @Column(name = "TIMBRADO_TELEFONO_4")
    private Integer timbradoTelefono4;
    @Column(name = "TIMBRADO_TELEFONO_5")
    private Integer timbradoTelefono5;
    
    @JoinColumns({
        @JoinColumn(name = "CONS_CAMPANA_ESTADOS_FK", referencedColumnName = "CONS"),
        @JoinColumn(name = "C_EMP", referencedColumnName = "c_emp", insertable = false, updatable = false)})
    @ManyToOne(optional = false, cascade={CascadeType.PERSIST, CascadeType.MERGE})
    private RbcCampanaEstado rbcCampanaEstados;
    @JoinColumns({
        @JoinColumn(name = "C_EMP", referencedColumnName = "C_EMP", insertable = false, updatable = false),
        @JoinColumn(name = "CONS_ESTADOS_FK", referencedColumnName = "CONS")})
    @ManyToOne(optional = false)
    private RbcEstado rbcEstados;

    public RbcCampana() {
    }

    public RbcCampana(RbcCampanaPK rbcCampanaPK) {
        this.rbcCampanaPK = rbcCampanaPK;
    }

    public RbcCampana(BigInteger cons, String cEmp) {
        this.rbcCampanaPK = new RbcCampanaPK(cons, cEmp);
    }

    public RbcCampanaPK getRbcCampanaPK() {
        return rbcCampanaPK;
    }

    public void setRbcCampanaPK(RbcCampanaPK rbcCampanaPK) {
        this.rbcCampanaPK = rbcCampanaPK;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Boolean getEnviarCorreo() {
        return enviarCorreo;
    }

    public void setEnviarCorreo(Boolean enviarCorreo) {
        this.enviarCorreo = enviarCorreo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRutaAudio() {
        return rutaAudio;
    }

    public void setRutaAudio(String rutaAudio) {
        this.rutaAudio = rutaAudio;
    }

    public String getTextoPreguntaCorreo() {
        return textoPreguntaCorreo;
    }

    public void setTextoPreguntaCorreo(String textoPreguntaCorreo) {
        this.textoPreguntaCorreo = textoPreguntaCorreo;
    }

    public String getTextoSiCorreo() {
        return textoSiCorreo;
    }

    public void setTextoSiCorreo(String textoSiCorreo) {
        this.textoSiCorreo = textoSiCorreo;
    }

    public String getTextoNoCorreo() {
        return textoNoCorreo;
    }

    public void setTextoNoCorreo(String textoNoCorreo) {
        this.textoNoCorreo = textoNoCorreo;
    }

    public Integer getIntentosTelefono1() {
        return intentosTelefono1;
    }

    public void setIntentosTelefono1(Integer intentosTelefono1) {
        this.intentosTelefono1 = intentosTelefono1;
    }

    public Integer getIntentosTelefono2() {
        return intentosTelefono2;
    }

    public void setIntentosTelefono2(Integer intentosTelefono2) {
        this.intentosTelefono2 = intentosTelefono2;
    }

    public Integer getIntentosTelefono3() {
        return intentosTelefono3;
    }

    public void setIntentosTelefono3(Integer intentosTelefono3) {
        this.intentosTelefono3 = intentosTelefono3;
    }

    public Integer getIntentosTelefono4() {
        return intentosTelefono4;
    }

    public void setIntentosTelefono4(Integer intentosTelefono4) {
        this.intentosTelefono4 = intentosTelefono4;
    }

    public Integer getIntentosTelefono5() {
        return intentosTelefono5;
    }

    public void setIntentosTelefono5(Integer intentosTelefono5) {
        this.intentosTelefono5 = intentosTelefono5;
    }

    public Integer getTimbradoTelefono1() {
        return timbradoTelefono1;
    }

    public void setTimbradoTelefono1(Integer timbradoTelefono1) {
        this.timbradoTelefono1 = timbradoTelefono1;
    }

    public Integer getTimbradoTelefono2() {
        return timbradoTelefono2;
    }

    public void setTimbradoTelefono2(Integer timbradoTelefono2) {
        this.timbradoTelefono2 = timbradoTelefono2;
    }

    public Integer getTimbradoTelefono3() {
        return timbradoTelefono3;
    }

    public void setTimbradoTelefono3(Integer timbradoTelefono3) {
        this.timbradoTelefono3 = timbradoTelefono3;
    }

    public Integer getTimbradoTelefono4() {
        return timbradoTelefono4;
    }

    public void setTimbradoTelefono4(Integer timbradoTelefono4) {
        this.timbradoTelefono4 = timbradoTelefono4;
    }

    public Integer getTimbradoTelefono5() {
        return timbradoTelefono5;
    }

    public void setTimbradoTelefono5(Integer timbradoTelefono5) {
        this.timbradoTelefono5 = timbradoTelefono5;
    }
    
    

    /**
	 * @return the rbcCampanaEstados
	 */
	public RbcCampanaEstado getRbcCampanaEstados() {
		return rbcCampanaEstados;
	}

	/**
	 * @return the rbcEstados
	 */
	public RbcEstado getRbcEstados() {
		return rbcEstados;
	}

	/**
	 * @param rbcCampanaEstados the rbcCampanaEstados to set
	 */
	public void setRbcCampanaEstados(RbcCampanaEstado rbcCampanaEstados) {
		this.rbcCampanaEstados = rbcCampanaEstados;
	}

	/**
	 * @param rbcEstados the rbcEstados to set
	 */
	public void setRbcEstados(RbcEstado rbcEstados) {
		this.rbcEstados = rbcEstados;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (rbcCampanaPK != null ? rbcCampanaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbcCampana)) {
            return false;
        }
        RbcCampana other = (RbcCampana) object;
        if ((this.rbcCampanaPK == null && other.rbcCampanaPK != null) || (this.rbcCampanaPK != null && !this.rbcCampanaPK.equals(other.rbcCampanaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ts.faces.beans.RbcCampana[ rbcCampanaPK=" + rbcCampanaPK + " ]";
    }
    
}
