/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TECHNISUPPORT
 */
@Entity(name="RbcEstado")
@Table(name = "RBC_ESTADOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RbcEstado.findAll", query = "SELECT r FROM RbcEstado r"),
    @NamedQuery(name = "RbcEstado.findByPk", query = "SELECT r FROM RbcEstado r WHERE r.rbcEstadosPK.cons = :cons AND r.rbcEstadosPK.cEmp = :cEmp")    
})
public class RbcEstado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbcEstadosPK rbcEstadosPK;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;


    public RbcEstado() {
    }

    public RbcEstado(RbcEstadosPK rbcEstadosPK) {
        this.rbcEstadosPK = rbcEstadosPK;
    }

    public RbcEstado(RbcEstadosPK rbcEstadosPK, String nombre, String descripcion) {
        this.rbcEstadosPK = rbcEstadosPK;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public RbcEstado(BigInteger cons, String cEmp) {
        this.rbcEstadosPK = new RbcEstadosPK(cons, cEmp);
    }

    public RbcEstadosPK getRbcEstadosPK() {
        return rbcEstadosPK;
    }

    public void setRbcEstadosPK(RbcEstadosPK rbcEstadosPK) {
        this.rbcEstadosPK = rbcEstadosPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rbcEstadosPK != null ? rbcEstadosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbcEstado)) {
            return false;
        }
        RbcEstado other = (RbcEstado) object;
        if ((this.rbcEstadosPK == null && other.rbcEstadosPK != null) || (this.rbcEstadosPK != null && !this.rbcEstadosPK.equals(other.rbcEstadosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.RbcEstados[ rbcEstadosPK=" + rbcEstadosPK + " ]";
    }
    
}
