/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ts.jamar.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.config.QueryHints;

/**
 *
 * @author TECHNISUPPORT
 */
@Entity
@Table(name = "RBC_CAMPANA_CARGA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RbcCampanaCarga.findAll", query = "SELECT r FROM RbcCampanaCarga r WHERE r.activo=TRUE"),
    @NamedQuery(name = "RbcCampanaCarga.findByCons", query = "SELECT r FROM RbcCampanaCarga r WHERE r.rbcCampanaCargaPK.cons = :cons AND r.activo=TRUE"),
    @NamedQuery(name = "RbcCampanaCarga.findByCEmp", query = "SELECT r FROM RbcCampanaCarga r WHERE r.rbcCampanaCargaPK.cEmp = :cEmp AND r.activo=TRUE")
    })
@NamedNativeQueries({
	@NamedNativeQuery(name="RbcCampanaCarga.findCustomAnswer", 
					  query="select"
					  		+ "  (select count(*) "
					  		+ "   from RBC_CAMPANA_CARGA"
					  		+ "   WHERE C_EMP= ?1 "
					  		+ "   AND CONS_CAMPANA_FK= ?2 "
					  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?3,'DD-MM-YYYY') and to_char(?4,'DD-MM-YYYY')  "
					  		+ "   AND  ACTIVO=1 "
					  		+ "  ) cantidad_clientes, "
					  		+ "  ( select count(*)"
					  		+ "    from RBC_CAMPANA_CARGA "
					  		+ "   WHERE C_EMP= ?5 "
					  		+ "   AND CONS_CAMPANA_FK= ?6 "
					  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?7,'DD-MM-YYYY') and to_char(?8,'DD-MM-YYYY')  "
					  		+ "   AND  ACTIVO=1 "
					  		+ "   AND EXITOSO =1)  cantidad_exito, "
					  		+ "  ( select count(*)"
					  		+ "    from RBC_CAMPANA_CARGA "
					  		+ "   WHERE C_EMP= ?9 "
					  		+ "   AND CONS_CAMPANA_FK= ?10 "
					  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?11,'DD-MM-YYYY') and to_char(?12,'DD-MM-YYYY')  "
					  		+ "   AND  ACTIVO=1 "
					  		+ "   AND EXITOSO =1)  cantidad_medio_exito"
					  		+ " from dual"),
				@NamedNativeQuery(name="RbcCampanaCarga.findSourceAnswer", 
				  query="select"
				  		+ "  (select count(*) " 
				  		+ "   from RBC_CAMPANA_CARGA"
				  		+ "   WHERE C_EMP= ?1 "
				  		+ "   AND CONS_CAMPANA_FK= ?2 "
				  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?3,'DD-MM-YYYY') and to_char(?4,'DD-MM-YYYY')  "
				  		+ "   AND  ACTIVO=1 "
				  		+ "  ) cantidad_clientes, "
				  		+ "  ( select count(*)"
				  		+ "    from RBC_CAMPANA_CARGA "
				  		+ "   WHERE C_EMP= ?5 "
				  		+ "   AND CONS_CAMPANA_FK= ?6 "
				  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?7,'DD-MM-YYYY') and to_char(?8,'DD-MM-YYYY')  "
				  		+ "   AND  ACTIVO=1 "
				  		+ "   AND EXITOSO =1)  cantidad_exito_llamada,"
				  		+ "  ( select count(*)"
				  		+ "    from RBC_CAMPANA_CARGA "
				  		+ "   WHERE C_EMP= ?9 "
				  		+ "   AND CONS_CAMPANA_FK= ?10 "
				  		+ "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?11,'DD-MM-YYYY') and to_char(?12,'DD-MM-YYYY')  "
				  		+ "   AND  ACTIVO=1 "
				  		+ "   AND MEDIO_EXITOSO =1)  cantidad_exito_correo"
				  		+ " from dual"),
				@NamedNativeQuery(name="RbcCampanaCarga.lastAttempts", 
				  query=    " select*"
	  					  + "	from (select NOMBRE,"
						  + "				( NVL(INTENTOS_TELEFONO_1, 0)+NVL(INTENTOS_TELEFONO_2, 0)+NVL(INTENTOS_TELEFONO_3, 0)+NVL(INTENTOS_TELEFONO_4, 0)+NVL(INTENTOS_TELEFONO_5, 0)) intentos,"
						  + "				CASE EXITOSO WHEN 1 THEN 'Exitoso'"
						  + "				ELSE 'No contesta' END exito_llamada ,"
						  + "				fecha_ejecucion"
						  + "				from RBC_CAMPANA_CARGA"
						  + "   WHERE C_EMP= ?1 "
						  + "   AND CONS_CAMPANA_FK= ?2 "
						  + "   AND  to_char(FECHA,'DD-MM-YYYY') BETWEEN to_char(?3,'DD-MM-YYYY') and to_char(?4,'DD-MM-YYYY')  "
						  + "   AND  ACTIVO=1 "
						  + " order by fecha_ejecucion desc)",
						  hints={@QueryHint(name=QueryHints.JDBC_MAX_ROWS, value="10")})

})

public class RbcCampanaCarga implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RbcCampanaCargaPK rbcCampanaCargaPK;
    @Column(name = "USUARIO")
    private String usuario;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "TELEFONO1")
    private String telefono1;
    @Column(name = "TELEFONO2")
    private String telefono2;
    @Column(name = "TELEFONO3")
    private String telefono3;
    @Column(name = "TELEFONO4")
    private String telefono4;
    @Column(name = "TELEFONO5")
    private String telefono5;
    @Column(name = "CORREO")
    private String correo;
    @Column(name = "INTENTOS_TELEFONO_1")
    private BigInteger intentosTelefono1;
    @Column(name = "INTENTOS_TELEFONO_2")
    private BigInteger intentosTelefono2;
    @Column(name = "INTENTOS_TELEFONO_3")
    private BigInteger intentosTelefono3;
    @Column(name = "INTENTOS_TELEFONO_4")
    private BigInteger intentosTelefono4;
    @Column(name = "INTENTOS_TELEFONO_5")
    private BigInteger intentosTelefono5;
    @Column(name = "EXITOSO")
    private Boolean exitoso;
    @Column(name = "MEDIO_EXITOSO")
    private Boolean medioExitoso;
    @Column(name = "RESPUESTA")
    private String respuesta;
    @Column(name = "ACTIVO")
    private Boolean activo;
    
    @JoinColumns({
        @JoinColumn(name = "CONS_CAMPANA_FK", referencedColumnName = "CONS"),
        @JoinColumn(name = "C_EMP", referencedColumnName = "C_EMP", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private RbcCampana rbcCampana;

    public RbcCampanaCarga() {
    }

    public RbcCampanaCarga(RbcCampanaCargaPK rbcCampanaCargaPK) {
        this.rbcCampanaCargaPK = rbcCampanaCargaPK;
    }

    public RbcCampanaCarga(BigInteger cons, String cEmp) {
        this.rbcCampanaCargaPK = new RbcCampanaCargaPK(cons, cEmp);
    }

    public RbcCampanaCargaPK getRbcCampanaCargaPK() {
        return rbcCampanaCargaPK;
    }

    public void setRbcCampanaCargaPK(RbcCampanaCargaPK rbcCampanaCargaPK) {
        this.rbcCampanaCargaPK = rbcCampanaCargaPK;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    public String getTelefono4() {
        return telefono4;
    }

    public void setTelefono4(String telefono4) {
        this.telefono4 = telefono4;
    }

    public String getTelefono5() {
        return telefono5;
    }

    public void setTelefono5(String telefono5) {
        this.telefono5 = telefono5;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public BigInteger getIntentosTelefono1() {
        return intentosTelefono1;
    }

    public void setIntentosTelefono1(BigInteger intentosTelefono1) {
        this.intentosTelefono1 = intentosTelefono1;
    }

    public BigInteger getIntentosTelefono2() {
        return intentosTelefono2;
    }

    public void setIntentosTelefono2(BigInteger intentosTelefono2) {
        this.intentosTelefono2 = intentosTelefono2;
    }

    public BigInteger getIntentosTelefono3() {
        return intentosTelefono3;
    }

    public void setIntentosTelefono3(BigInteger intentosTelefono3) {
        this.intentosTelefono3 = intentosTelefono3;
    }

    public BigInteger getIntentosTelefono4() {
        return intentosTelefono4;
    }

    public void setIntentosTelefono4(BigInteger intentosTelefono4) {
        this.intentosTelefono4 = intentosTelefono4;
    }

    public BigInteger getIntentosTelefono5() {
        return intentosTelefono5;
    }

    public void setIntentosTelefono5(BigInteger intentosTelefono5) {
        this.intentosTelefono5 = intentosTelefono5;
    }

    public Boolean getExitoso() {
        return exitoso;
    }

    public void setExitoso(Boolean exitoso) {
        this.exitoso = exitoso;
    }

    public Boolean getMedioExitoso() {
        return medioExitoso;
    }

    public void setMedioExitoso(Boolean medioExitoso) {
        this.medioExitoso = medioExitoso;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
    

    /**
	 * @return the rbcCampana
	 */
	public RbcCampana getRbcCampana() {
		return rbcCampana;
	}

	/**
	 * @param rbcCampana the rbcCampana to set
	 */
	public void setRbcCampana(RbcCampana rbcCampana) {
		this.rbcCampana = rbcCampana;
	}
	
	/**
	 * @return the activo
	 */
	public Boolean getActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (rbcCampanaCargaPK != null ? rbcCampanaCargaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RbcCampanaCarga)) {
            return false;
        }
        RbcCampanaCarga other = (RbcCampanaCarga) object;
        if ((this.rbcCampanaCargaPK == null && other.rbcCampanaCargaPK != null) || (this.rbcCampanaCargaPK != null && !this.rbcCampanaCargaPK.equals(other.rbcCampanaCargaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ts.faces.beans.RbcCampanaCarga[ rbcCampanaCargaPK=" + rbcCampanaCargaPK + " ]";
    }
    
}
